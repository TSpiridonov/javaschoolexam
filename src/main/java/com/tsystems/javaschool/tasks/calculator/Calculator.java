package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    public static void main(String[] args) {
        Calculator c = new Calculator();
        System.out.println(c.evaluate("(1+38)*4-5")); // Result: 151
        System.out.println(c.evaluate("7*6/2+8")); // Result: 29
        System.out.println(c.evaluate("-12)1//(")); // Result: null
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (!isStatementOK(statement)) {
            return null;
        }

        double x = getResult(transformStatement(statement));

        if (Double.valueOf(x).isInfinite()) {
            return null;
        }

        int y = (int) x;
        if (x - y == 0) {
            return Integer.toString(y);
        }

        return Double.toString(x);
    }

    public boolean isStatementOK(String statement) {

        if (statement == null || statement.isEmpty()) {
            return false;
        }

        int openBracketsCount = 0;
        int closedBracketsCount = 0;

        for (char ch : statement.toCharArray()) {
            if (ch == '(') {
                openBracketsCount++;
            }
            if (ch == ')') {
                closedBracketsCount++;
            }
        }

        if (openBracketsCount != closedBracketsCount && (openBracketsCount + closedBracketsCount) % 2 != 0) {
            return false;
        }
        return !statement.contains("++") &&
                !statement.contains("--") &&
                !statement.contains("**") &&
                !statement.contains("//") &&
                !statement.contains("..") &&
                !statement.contains(",");
    }

    public String transformStatement(String statement) {
        StringBuilder result = new StringBuilder();
        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < statement.length(); i++) {
            if (getPriority(statement.charAt(i)) == 0) {
                result.append(statement.charAt(i));
            }
            if (getPriority(statement.charAt(i)) == 1) {
                stack.push(statement.charAt(i));
            }
            if (getPriority(statement.charAt(i)) > 1) {
                result.append(" ");
                while (!stack.empty()) {
                    if (getPriority(stack.peek()) >= getPriority(statement.charAt(i))) {
                        result.append(stack.pop());
                    } else {
                        break;
                    }
                }
                stack.push(statement.charAt(i));
            }
            if (getPriority(statement.charAt(i)) == -1) {
                result.append(" ");
                while (getPriority(stack.peek()) != 1) {
                    result.append(stack.pop());
                }
                stack.pop();
            }
        }

        while (!stack.empty()) {
            result.append(" ");
            result.append(stack.pop());
        }
        return result.toString();
    }

    public double getResult(String statement) {
        StringBuilder result = new StringBuilder();
        Stack<Double> stack = new Stack<>();
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == ' ') continue;
            if (getPriority(statement.charAt(i)) == 0) {
                while (statement.charAt(i) != ' ') {
                    result.append(statement.charAt(i++));
                    if (i == statement.length()) break;
                }
                stack.push(Double.parseDouble(result.toString()));
                result = new StringBuilder();
            }
            if (getPriority(statement.charAt(i)) > 1) {
                double x = stack.pop(), y = stack.pop();

                switch (statement.charAt(i)) {
                    case '+':
                        stack.push(y + x);
                        break;
                    case '-':
                        stack.push(y - x);
                        break;
                    case '*':
                        stack.push(y * x);
                        break;
                    case '/':
                        stack.push(y / x);
                        break;
                }
            }
        }
        return stack.pop();
    }


    public int getPriority(Character ch) {

        switch (ch) {
            case '+':
            case '-':
                return 2;
            case '*':
            case '/':
                return 3;
            case '(':
                return 1;
            case ')':
                return -1;
            default:
                return 0;
        }

    }

}
