package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers.contains(null) || inputNumbers.size() < 3) {
            throw new CannotBuildPyramidException();
        }


        int size = inputNumbers.size();
        int height = 0;
        while (size > 0) {
            size -= height + 1;
            if (size < 0) {
                throw new CannotBuildPyramidException();
            } else {
                height++;
            }

        }
        int width = height * 2 - 1;

        int[][] result = new int[height][width];
        Collections.sort(inputNumbers);


        int index = 0;
        for (int i = 0; i < height; i++) {
            int l = height - i - 1;
            for (int j = 0; j <= i; j++) {
                result[i][l] = inputNumbers.get(index);
                index++;
                l += 2;
            }
        }

        return result;
    }

}
